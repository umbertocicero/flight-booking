app.controller('choseSeatController', [
		'$scope',
		'$http',
		'$location',
		'$routeParams',
		'BookingService',
		function($scope, $http, $location, $routeParams, BookingService) {
			$scope.flightId = $routeParams.flightId;

			$scope.categoryList = [ {}, {
				color : 'primary'
			}, {
				color : 'info'
			}, {
				color : 'warning'
			}, {
				color : 'success'
			} ];

			$scope.flight = null;
			$scope.matrix = [];
			$scope.selectedSeat = null;

			$scope.selectSeat = function(item) {
				$scope.selectedSeat = item;

				BookingService.getPrice($scope.flightId, item, function(
						response) {
					item.realPrice = response.data.price;
				}, function(response) {
				});

			}

			BookingService.searchFlight($scope.flightId, function(response) {
				$scope.flight = response.data;
			}, function(response) {
				$scope.flight=null;
			});

			var populateCategory = function(item) {
				BookingService.getCategory(item._links.category.href, function(
						response) {
					item.category = response.data;
					$scope.matrix[item.srow - 1][item.scolumn - 1] = item;
				}, function(response) {
				});
			}

			var getSeatsSuccessCallback = function(response) {
				var seats = response.data._embedded.seats;

				$scope.matrix = []
				for (var i = 0; i < 8; i++) {
					var row = [];
					for (j = 0; j < 4; j++) {
						row.push({});
					}
					$scope.matrix.push(row);
				}

				for (var i = 0; i < seats.length; i++) {
					var item = seats[i];

					populateCategory(item);

					$scope.matrix[item.srow - 1][item.scolumn - 1] = item;
				}
			}

			BookingService.getSeats($scope.flightId, getSeatsSuccessCallback,
					function(response) {
						$scope.matrix = [];
					});

			$scope.goToBooking = function(pnr){
				var url = '/booking/' + pnr;
				$location.path(url);
				$scope.$apply();
			}
			$scope.booking = function() {
				var seat = $scope.selectedSeat;
				var flightId = $scope.flightId;
				var seatDto = {
					id : seat.id,
					realPrice : seat.realPrice,
					flight : {
						id : flightId
					}
				};
				BookingService.booking(seatDto, function(response) {
					var pnr = response.data.pnr;
					swal('Thank You!',
							'Thanks for booking! You PNR code is: ' + pnr,
							'success').then(function () {
								$scope.goToBooking(pnr);
										}, function (dismiss) {
											$scope.goToBooking(pnr);
										});
							
				}, function(response) {
					swal(
							  'Oops...',
							  'Something went wrong!',
							  response
							)
				});

			};
			

			$scope.bookingMsg = function() {
				swal({
					title : 'Are you sure you want to proceed?',
					type : 'warning',
					showCancelButton : true,
					confirmButtonColor : '#3085d6',
					cancelButtonColor : '#d33',
					confirmButtonText : 'Yes!'
				}).then(function() {
					$scope.booking();
				}).catch(swal.noop);
			}

		} ]);
