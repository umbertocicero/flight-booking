app.controller('bookingController', [
		'$scope',
		'$routeParams',
		'$location',
		'BookingService',
		function($scope, $routeParams, $location, BookingService) {

			$scope.pnrId = $routeParams.pnrId;

			$scope.booking = null;

			$scope.searchBooking = function() {

				var success = function(response) {
					$scope.booking = response;

					BookingService.getCategory(
							$scope.booking.seat._links.category.href, function(
									response) {
								$scope.booking.seat.category = response.data;
							}, function(response) {
							});
					BookingService.searchFlightByUrl(
							$scope.booking.seat._links.flight.href, function(
									response) {
								$scope.booking.flight = response.data;
							}, function(response) {
							});

				}
				var error = function(response) {
				};
				BookingService.searchBooking($scope.pnrId, success, error);

			}

			if ($scope.pnrId) {
				$scope.searchBooking();
			}

		} ]);
