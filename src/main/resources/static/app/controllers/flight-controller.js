app.controller('flightController', [
		'$scope',
		'$http',
		'$location',
		'BookingService',
		function($scope, $http, $location, BookingService) {
			$scope.search = {
				departure : 'Firenze',
				destination : 'Milano',
				date : new Date(2017, 10, 1)
			};

			$scope.dataset = [];
			$scope.showPanel = false;

			$scope.goToSeat = function(flightId) {
				var url = '/choseSeat/' + flightId;
				$location.path(url);
			}
			Date.prototype.addDays = function(days) {
				var dat = new Date(this.valueOf());
				dat.setDate(dat.getDate() + days);
				return dat;
			}

			$scope.searchFlights = function() {
				var start = $scope.search.date;
				start.setMinutes(0);
				start.setHours(0);
				start.setSeconds(0);

				var end = $scope.search.date;
				end.setMinutes(0);
				end.setHours(0);
				end.setSeconds(0);
				end = end.addDays(1);

				var offset = start.getTimezoneOffset();
				var startL = start.getTime() - (offset * 60 * 1000);
				var endL = end.getTime() - (offset * 60 * 1000);

				var params = {
					departure : $scope.search.departure,
					destination : $scope.search.destination,
					start : startL,
					end : endL,
				}

				var success = function(response) {
					$scope.dataset = response.data._embedded.flights;
					$scope.showPanel = $scope.dataset.length == 0 ? true
							: false;
				}
				var error = function(response) {
					$scope.dataset = [];
					$scope.showPanel = true;
				};
				BookingService.searchFlights(params, success, error);

			}

		} ]);
