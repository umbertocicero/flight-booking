function BookingService($http, SpringDataRestAdapter) {
	return {
		searchFlights : function(params, successCallback, errorCallback) {
			return $http({
				url : '/flights/search/findByDepartureAndDestinationAndDepartureDateBetween',
				method : "GET",
				params : params
			}).then(function(response) {
				successCallback(response);
			}, function(response) {
				errorCallback(response);
			});
		},

		searchFlight : function(flightId, successCallback, errorCallback) {
			return $http({
				url : '/flights/' + flightId,
				method : "GET"
			}).then(function(response) {
				successCallback(response);
			}, function(response) {
				errorCallback(response);
			});
		},
		searchFlightByUrl : function(url, successCallback, errorCallback) {
			return $http({
				url : url,
				method : "GET"
			}).then(function(response) {
				successCallback(response);
			}, function(response) {
				errorCallback(response);
			});
		},
		searchBooking : function(pnrId, successCallback, errorCallback) {
			var httpPromise = $http({
				url : '/bookings/search/findByPnr?pnr=' + pnrId,
				method : "GET"
			});

			var sdr = SpringDataRestAdapter
					.process(httpPromise)
					.then(
							function(processedResponse) {
								var booking = processedResponse._embeddedItems[0];
								var availableResources = processedResponse
										._resources();

								var parentSeatResource = booking
										._resources("seat");

								var seat = parentSeatResource.get(function() {
									booking.seat = seat;
									successCallback(booking);
								});
							}, function(response) {
								errorCallback(response);
							});

			return sdr;
		},

		getPrice : function(flightId, seat, successCallback, errorCallback) {
			$http({
				url : '/getPrice/' + flightId + '/' + seat.id,
				method : "GET"
			}).then(function(response) {
				successCallback(response);
			}, function(response) {
				errorCallback(response);
			});
		},
		getSeats : function(flightId, successCallback, errorCallback) {
			$http({
				url : '/flights/' + flightId + '/seats/',
				method : "GET"
			}).then(function(response) {
				successCallback(response);
			}, function(response) {
				errorCallback(response);
			});
		},
		getCategory : function(url, successCallback, errorCallback) {
			$http({
				url : url,
				method : "GET"
			}).then(function(response) {
				successCallback(response);
			}, function(response) {
				errorCallback(response);
			});
		},
		booking : function(seat, successCallback, errorCallback) {

			var req = {
				method : 'POST',
				url : '/booking',
				headers : {
					'Content-Type' : 'application/json;'
				},
				data : seat
			}

			$http(req).then(function(response) {
				successCallback(response);
			}, function(response) {
				errorCallback(response);
			});
		}

	}
}
app.factory('BookingService', BookingService);