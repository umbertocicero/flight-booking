var app = angular.module('app', [ 'ngRoute', 'ngResource','spring-data-rest' ]);
app.config(function($routeProvider) {
	$routeProvider.when('/search', {
		templateUrl : '/views/flight.html',
		controller : 'flightController'
	}).when('/choseSeat/:flightId', {
		templateUrl : '/views/choseSeat.html',
		controller : 'choseSeatController'
	}).when('/booking', {
		templateUrl : '/views/booking.html',
		controller : 'bookingController'
	}).when('/booking/:pnrId', {
		templateUrl : '/views/booking.html',
		controller : 'bookingController'
	}).otherwise('/search', {
		templateUrl : '/views/flight.html',
		controller : 'flightController'
	});
});
