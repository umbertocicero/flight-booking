package it.uac.thales.rest.dto;

import it.uac.thales.database.pojo.Seat;

public class SeatDto extends Seat{
	public Float realPrice;

	public Float getRealPrice() {
		return realPrice;
	}

	public void setRealPrice(Float realPrice) {
		this.realPrice = realPrice;
	}
	
}
