package it.uac.thales.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import it.uac.thales.database.pojo.Booking;
import it.uac.thales.database.repository.CustomizedBookingRepository;
import it.uac.thales.rest.dto.SeatDto;

@Controller
public class RestController {

	@Autowired
	private CustomizedBookingRepository customizedBookingRepository;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView method() {
		return new ModelAndView("redirect:index.html");
	}

	@RequestMapping(value = "/getPrice/{flight_id}/{seat_id}", method = RequestMethod.GET)
	public @ResponseBody SeatDto getPrice(@PathVariable("flight_id") long flight_id, @PathVariable("seat_id") long seat_id) {

		Float result = customizedBookingRepository.getPrice(flight_id, seat_id);

		SeatDto dto = new SeatDto();
		dto.setPrice(result);

		return dto;
	}

	@RequestMapping(value = "/booking", method = RequestMethod.POST)
	public ResponseEntity<Booking> booking(@RequestBody SeatDto seatDto) {
		HttpHeaders responseHeaders = new HttpHeaders();

		Booking result = new Booking();
		try {
			Booking bResult = customizedBookingRepository.saveBooking(seatDto);
			result.setId(bResult.getId());
			result.setPnr(bResult.getPnr());
			result.setPrice(bResult.getPrice());
		} catch (Exception e) {
			return new ResponseEntity<>(null, responseHeaders, HttpStatus.CONFLICT);
		}

		return new ResponseEntity<>(result, responseHeaders, HttpStatus.OK);
	}

}
