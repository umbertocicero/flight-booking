package it.uac.thales;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan("it.uac.thales")
public class FlightBooking{

    public static void main(String[] args) throws Exception{
    	System.setProperty("user.timezone", "GMT");
        SpringApplication.run(FlightBooking.class, args);
    }
}

