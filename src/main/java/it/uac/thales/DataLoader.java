package it.uac.thales;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import it.uac.thales.database.repository.InitRepository;

@Component
public class DataLoader implements ApplicationRunner {

    private InitRepository intRepository;

    @Autowired
    public DataLoader(InitRepository intRepository) {
        this.intRepository = intRepository;
    }

    public void run(ApplicationArguments args) {
    	intRepository.init();
    }
}