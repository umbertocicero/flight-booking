package it.uac.thales.database.pojo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Seat")
public class Seat {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long Id;
	@Column(nullable = false)
	private Integer srow, scolumn;
	
	private Float price;

	@ManyToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "category_id")
	private Category category;
	
	@OneToOne(mappedBy = "seat")
	private Booking booking;

	private Integer status = 0;

	@ManyToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "flight_id")
	private Flight flight;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Integer getSrow() {
		return srow;
	}

	public void setSrow(Integer srow) {
		this.srow = srow;
	}

	public Integer getScolumn() {
		return scolumn;
	}

	public void setScolumn(Integer scolumn) {
		this.scolumn = scolumn;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Booking getBooking() {
		return booking;
	}

	public void setBooking(Booking booking) {
		this.booking = booking;
	}
}
