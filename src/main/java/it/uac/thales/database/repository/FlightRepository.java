package it.uac.thales.database.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import it.uac.thales.database.pojo.Flight;

@RepositoryRestResource(collectionResourceRel = "flights", path = "flights")
public interface FlightRepository extends PagingAndSortingRepository<Flight, Long>{
	List<Flight> findByDeparture(@Param("departure") String departure);

	List<Flight> findByDepartureAndDestinationAndDepartureDateBetween(@Param("departure") String departure, @Param("destination") String destination, @Param("start") BigInteger start,  @Param("end") BigInteger end);
	List<Flight> findByDepartureAndDestinationAndDepartureDateGreaterThanEqualAndDepartureDateLessThan(@Param("departure") String departure, @Param("destination") String destination, @Param("start") BigInteger start,  @Param("end") BigInteger end);
}
