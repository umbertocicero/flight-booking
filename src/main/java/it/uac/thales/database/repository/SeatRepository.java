package it.uac.thales.database.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import it.uac.thales.database.pojo.Flight;
import it.uac.thales.database.pojo.Seat;

@RepositoryRestResource(collectionResourceRel = "seats", path = "seats")
public interface SeatRepository extends PagingAndSortingRepository<Seat, Long> {
	List<Seat> findByFlight(@Param("flight") Flight flight);
	long countByFlightAndStatus(@Param("flight") Flight flight, @Param("status") Integer status);
}
