package it.uac.thales.database.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import it.uac.thales.database.pojo.Booking;

@RepositoryRestResource(collectionResourceRel = "bookings", path = "bookings")
public interface BookingRepository extends PagingAndSortingRepository<Booking, Long> {
	List<Booking> findByPnr(@Param("pnr") String pnr);
}
