package it.uac.thales.database.repository;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import it.uac.thales.database.pojo.Category;
import it.uac.thales.database.pojo.Flight;
import it.uac.thales.database.pojo.Seat;

@Repository
public class InitRepository {

	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private FlightRepository flightRepository;
	@Autowired
	private SeatRepository seatRepository;

	@Transactional
	public void init() {

		Category category = categoryRepository.findOne(1L);
		if (category != null) {
			return;
		}
		
		createCategories();

		Flight flight = createFlight("Firenze", "Milano", "1509526800000");
		createSeats(flight);

		Flight flight2 = createFlight("Firenze", "Milano", "1509568200000");
		createSeats(flight2);

		Flight flight3 = createFlight("Roma", "Milano", "1509568200000");
		createSeats(flight3);

		Flight flight4 = createFlight("Milano", "Roma", "1509632400000");
		createSeats(flight4);

		Flight flight5 = createFlight("Firenze", "Milano", "1509632400000");
		createSeats(flight5);

		Flight flight6 = createFlight("Roma", "Milano", "1509526800");
		createSeats(flight6);

		Flight flight7 = createFlight("Roma", "Milano", "1507734900000");
		createSeats(flight7);

		Flight flight8 = createFlight("Milano", "Firenze", "1507734900000");
		createSeats(flight8);
	}

	private void createCategories() {
		addCategory(1L, "First Class", 80F);
		addCategory(2L, "Second Class", 60F);
		addCategory(3L, "Business", 90F);
		addCategory(4L, "Economy", 55F);
	}

	private void addCategory(Long id, String description, Float maxPrice) {
		Category category = new Category();
		category.setId(id);
		category.setDescription(description);
		category.setMaxPrice(maxPrice);
		categoryRepository.save(category);
	}

	private Flight createFlight(String departure, String destination, String departureDate) {
		Flight flight = new Flight();
		flight.setDeparture(departure);
		flight.setDestination(destination);
		flight.setDepartureDate(new BigInteger(departureDate));
		return flightRepository.save(flight);
	}

	private void createSeats(Flight flight) {

		Category first = categoryRepository.findOne(1L);
		Category second = categoryRepository.findOne(2L);
		Category business = categoryRepository.findOne(3L);
		Category economy = categoryRepository.findOne(4L);

		Set<Seat> seats = new HashSet<>();
		for (int i = 1; i < 5; i++) {
			addSeat(seats, flight, business, 1, i, 50F);
		}
		for (int i = 1; i < 5; i++) {
			addSeat(seats, flight, first, 2, i, 30F);
		}
		for (int i = 3; i < 6; i++) {
			for (int j = 1; j < 5; j++) {
				addSeat(seats, flight, second, i, j, 20F);
			}
		}
		for (int i = 6; i < 9; i++) {
			for (int j = 1; j < 5; j++) {
				addSeat(seats, flight, economy, i, j, 15F);
			}
		}

		seatRepository.save(seats);
	}

	private void addSeat(Set<Seat> seats, Flight flight, Category category, int row, int column, Float price) {
		Seat seat = new Seat();
		seat.setCategory(category);
		seat.setScolumn(column);
		seat.setSrow(row);
		seat.setFlight(flight);
		seat.setPrice(price);
		seats.add(seat);
	}

}
