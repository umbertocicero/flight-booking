package it.uac.thales.database.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import it.uac.thales.database.pojo.Booking;
import it.uac.thales.database.pojo.Category;
import it.uac.thales.database.pojo.Flight;
import it.uac.thales.database.pojo.Seat;
import it.uac.thales.rest.dto.SeatDto;

@Repository
public class CustomizedBookingRepository {

	@Autowired
	private BookingRepository bookingRepository;
	@Autowired
	private SeatRepository seatRepository;

	public Float getPrice(long flight_id, long seat_id) {
		// SeatPrice := Min ( ServiceClassBasePrice * (1 + availabilityAdjustmentFactor) , ServiceClassMaxPrice )

		Flight f = new Flight();
		f.setId(flight_id);

		long count = seatRepository.countByFlightAndStatus(f, 1);

		Seat seat = seatRepository.findOne(seat_id);
		Category category = seat.getCategory();
		float availabilityAdjustmentFactor = count / 60F;
		Float min = Math.min(seat.getPrice() * (1 + availabilityAdjustmentFactor), category.getMaxPrice());

		Integer value = min.intValue();
		return value.floatValue();
	}

	public Booking saveBooking(SeatDto seatDto) throws Exception {
		Seat seat = seatRepository.findOne(seatDto.getId());
		if (!seat.getStatus().equals(0)) {
			throw new Exception("Already reserved");
		}

		Flight flight = seat.getFlight();

		String firstLetter = String.valueOf(flight.getDeparture().charAt(0));
		String secondLetter = String.valueOf(flight.getDestination().charAt(0));

		Booking booking = new Booking();
		booking.setPrice(seatDto.getRealPrice());
		booking.setSeat(seat);

		String pnr = String.format("PRN%s%s%s%s", flight.getId(), firstLetter, seat.getId(), secondLetter);
		booking.setPnr(pnr);

		Booking bResult = bookingRepository.save(booking);

		seat.setBooking(bResult);
		seat.setStatus(1);

		seatRepository.save(seat);

		return bResult;
	}
}
